
### To Run Differences

 - ruby setup.rb
 - ruby entry.rb

### To Run Tests
- ENV=test ruby setup.rb
- ENV=test bundle exec rspec
