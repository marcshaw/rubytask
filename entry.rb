$LOAD_PATH.unshift File.expand_path(".", "lib/services/")
$LOAD_PATH.unshift File.expand_path(".", "lib/models/")
$LOAD_PATH.unshift File.expand_path(".", "lib/services/mockbin/")

require 'detect_discrepancies'

puts DetectDiscrepancies.new.call
