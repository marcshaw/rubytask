require 'active_record'
require 'securerandom'
require_relative 'lib/database_connection'
require_relative 'lib/models/campaign'
require 'byebug'

ActiveRecord::Schema.define do
  self.verbose = true

  create_table(:campaigns) do |t|
    t.string :job_id, null: false
    t.string :external_reference, null: false
    t.string :status, null: false
    t.text :ad_description, null: false
    t.datetime :created_at, null: false
  end
end

env = ENV.fetch("ENV", "dev")

if(env == 'dev')
  Campaign.create!(job_id: SecureRandom.hex(15), external_reference: '1', status: Campaign::ACTIVE, ad_description: 'Description for campaign 11')
  Campaign.create!(job_id: SecureRandom.hex(15), external_reference: '2', status: Campaign::ACTIVE, ad_description: 'Description for campaign 12')
  Campaign.create!(job_id: SecureRandom.hex(15), external_reference: '3', status: Campaign::ACTIVE, ad_description: 'Description for campaign 20')
end
