require 'retrieve_data'
require 'calculate_differences'
require 'display_differences'

class DetectDiscrepancies
  def self.call
    new.call
  end

  def call
    parsed_data = Mockbin::RetrieveData.call
    differences = CalculateDifferences.call(parsed_data: parsed_data)
    DisplayDifferences.call(differences: differences)
  end
end
