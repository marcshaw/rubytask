require 'http'
require_relative '../../models/campaign'

module Mockbin
  class RetrieveData
    URL = 'https://mockbin.org/bin/fcb30500-7b98-476f-810d-463a0b8fc3df'

    def self.call
      new.call
    end

    def call
      data = call_api
      parse_data(data)
    end

    private

    def call_api
      HTTP.get(URL).to_s
    end

    def parse_data(data)
      JSON.parse(data)["ads"].map do |ad|
        {
          external_reference: ad['reference'],
          status: translate_status(ad['status']),
          ad_description: ad['description']
        }.with_indifferent_access
      end
    end

    def translate_status(status)
      case status
      when 'enabled'
        Campaign::ACTIVE
      when 'disabled'
        Campaign::PAUSED
      when 'deleted'
        Campaign::DELETED
      end
    end
  end
end
