class DisplayDifferences
  def self.call(args)
    new(**args).call
  end

  def initialize(differences:)
    @differences = differences
  end

  def call
    differences.map do |diff|
      discrepancies = calculate_discrepancies(diff)

      if discrepancies.blank?
        nil
      else
        {
          remote_reference: diff[:ext_ref],
          discrepanices: discrepancies
        }
      end
    end.compact
  end

  private

  attr_reader :differences

  def calculate_discrepancies(diff)
    diff[:differences].map do |attribute|
      data = { remote: diff[:external][attribute], internal: diff[:internal].attributes[attribute] }
      Hash[attribute.to_sym, data]
    end
  end
end
