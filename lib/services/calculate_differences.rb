require_relative '../models/campaign'

class CalculateDifferences
  def self.call(args)
    new(**args).call
  end

  def initialize(parsed_data:)
    @parsed_data = parsed_data
  end

  def call
    raise ArgumentError, "Parsed data is invalid" if parsed_data.blank?

    matching_campaigns.map do |campaign|
      external_campaign = matching_external_campaign(campaign.external_reference)

      calculate_differences(campaign, external_campaign)
    end
  end

  private

  attr_reader :parsed_data

  def matching_campaigns
    external_refs = parsed_data.map { |x| x[:external_reference] }
    Campaign.where(external_reference: external_refs)
  end

  def matching_external_campaign(ext_ref)
    parsed_data.find { |data| data[:external_reference].to_i == ext_ref.to_i }
  end

  def calculate_differences(campaign, external_campaign)
    differences = []
    differences << 'status' if external_campaign[:status] != campaign.status
    differences << 'ad_description' if external_campaign[:ad_description] != campaign.ad_description

    {
      ext_ref: external_campaign[:external_reference],
      internal: campaign,
      external: external_campaign,
      differences: differences
    }
  end
end
