require 'active_record'
require_relative '../database_connection'

class Campaign < ActiveRecord::Base
  ACTIVE = 'active'.freeze
  PAUSED = 'paused'.freeze
  DELETED = 'deleted'.freeze
  STATUSES = [ACTIVE, PAUSED, DELETED].freeze
end
