require 'active_record'

env = ENV.fetch("ENV", "dev")

db_config = {
  'adapter' => 'sqlite3',
  'database' => "db/heyjobs_#{env}"
}

ActiveRecord::Base.establish_connection(db_config)
