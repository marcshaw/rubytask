require_relative '../spec_helper'
require 'detect_discrepancies'

describe DetectDiscrepancies, type: :integration do
  subject(:detect_discrepancies) { described_class.call }
  let!(:campaign_1) { create(:campaign, campaign_1_data) }
  let!(:campaign_2) { create(:campaign, campaign_2_data) }
  let(:campaign_1_data) { { external_reference: 1, status: Campaign::ACTIVE, ad_description: "this is a test" } }
  let(:campaign_2_data) { { external_reference: 2, status: Campaign::PAUSED, ad_description: "this is a test too" } }
  let(:http_result) do
    {
      "ads": [
        {
          "reference": "1",
          "status": "enabled",
          "description": "this is a test"
        },
        {
          "reference": "2",
          "status": "disabled",
          "description": "Description for campaign 12"
        }
      ]
    }.to_json
  end
  let(:expected_result) do
    [
      {
        remote_reference: "2",
        discrepanices: [
          {
            ad_description: {
              internal: "this is a test too",
              remote: "Description for campaign 12"
            }
          }
        ]
      }
    ]
  end

  before do
    allow(HTTP).to receive(:get).and_return(http_result)
  end

  it 'returns the expected result' do
    expect(detect_discrepancies).to match(expected_result)
  end
end
