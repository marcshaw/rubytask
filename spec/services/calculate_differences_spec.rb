require_relative '../spec_helper'
require 'calculate_differences'

describe CalculateDifferences, type: :service do
  subject(:calculate_differences) { described_class.call(parsed_data: parsed_data) }

  context 'when parsed data is valid' do
    let(:parsed_data) do
      [
        { external_reference: 1, status: Campaign::ACTIVE, ad_description: "this is a test" },
        { external_reference: 2, status: Campaign::PAUSED, ad_description: "this is a test too" }
      ]
    end

    context 'when there are matching campaigns' do
      let!(:campaign_1) { create(:campaign, campaign_1_data) }
      let!(:campaign_2) { create(:campaign, campaign_2_data) }

      context 'when there are no differences' do
        let(:campaign_1_data) { parsed_data.first }
        let(:campaign_2_data) { parsed_data.second }
        let(:expected_result) do
          [
            {
              ext_ref: 1,
              internal: campaign_1,
              external: {
                ad_description: "this is a test",
                external_reference: 1,
                status: Campaign::ACTIVE
              },
              differences: [],
            },
            {
              ext_ref: 2,
              internal: campaign_2,
              external: {
                ad_description: "this is a test too",
                external_reference: 2,
                status: Campaign::PAUSED
              },
              differences: [],
            }
          ]
        end

        it 'returns an empty array for the differences' do
          expect(calculate_differences).to eq(expected_result)
        end
      end

      context 'when there are differences' do
        let(:campaign_1_data) { { external_reference: 1, status: Campaign::ACTIVE, ad_description: "this is a test!!" } }
        let(:campaign_2_data) { { external_reference: 2, status: Campaign::ACTIVE, ad_description: "this is a test" } }
        let(:expected_result) do
          [
            {
              ext_ref: 1,
              internal: campaign_1,
              external: {
                ad_description: "this is a test",
                external_reference: 1,
                status: Campaign::ACTIVE
              },
              differences: ['ad_description'],
            },
            {
              ext_ref: 2,
              internal: campaign_2,
              external: {
                ad_description: "this is a test too",
                external_reference: 2,
                status: Campaign::PAUSED
              },
              differences: ['status', 'ad_description'],
            }
          ]
        end

        it 'returns the expected differences' do
          expect(calculate_differences).to eq(expected_result)
        end
      end
    end

    context 'when there are no matching campaigns' do
      it 'returns an empty array' do
        expect(calculate_differences).to eq([])
      end
    end
  end

  context 'when parsed data is invalid' do
    context 'when it is nil' do
      let(:parsed_data) { nil }

      it 'raises an argument error' do
        expect { calculate_differences }.to raise_error(ArgumentError, 'Parsed data is invalid')
      end
    end

    context 'when it is empty' do
      let(:parsed_data) { [] }

      it 'raises an argument error' do
        expect { calculate_differences }.to raise_error(ArgumentError, 'Parsed data is invalid')
      end
    end
  end
end
