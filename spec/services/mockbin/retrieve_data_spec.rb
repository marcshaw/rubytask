require_relative '../../spec_helper'
require 'retrieve_data'

describe Mockbin::RetrieveData, type: :service do
  subject(:retrieve_data) { described_class.call }
  let(:expected_result) do
    [
      {
        ad_description: "Description for campaign 11",
        external_reference: "1",
        status: Campaign::ACTIVE
      },
      {
        ad_description: "Description for campaign 12",
        external_reference: "2",
        status: Campaign::PAUSED
      },
      {
        ad_description: "Description for campaign 13",
        external_reference: "3",
        status: Campaign::DELETED
      }
    ]
  end

  let(:http_result) do
    {
      "ads": [
        {
          "reference": "1",
          "status": "enabled",
          "description": "Description for campaign 11"
        },
        {
          "reference": "2",
          "status": "disabled",
          "description": "Description for campaign 12"
        },
        {
          "reference": "3",
          "status": "deleted",
          "description": "Description for campaign 13"
        }
      ]
    }.to_json
  end

  before do
    allow(HTTP).to receive(:get).and_return(http_result)
  end

  it 'returns the expected parsed data' do
    expect(retrieve_data).to match(expected_result)
  end
end
