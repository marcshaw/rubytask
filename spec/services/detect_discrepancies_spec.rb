require_relative '../spec_helper'
require 'detect_discrepancies'

describe DetectDiscrepancies, type: :service do
  subject(:detect_discrepancies) { described_class.call }
  let(:data) { double }
  let(:differences) { double }
  let(:display) { double }

  it 'calls the expected services and returns the result' do
    expect(Mockbin::RetrieveData).to receive(:call).and_return(data)
    expect(CalculateDifferences).to receive(:call).with(parsed_data: data).and_return(differences)
    expect(DisplayDifferences).to receive(:call).with(differences: differences).and_return(display)

    expect(detect_discrepancies).to eq(display)
  end
end
