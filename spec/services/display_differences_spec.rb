require_relative '../spec_helper'
require 'calculate_differences'

describe DisplayDifferences, type: :service do
  subject(:display_differences) { described_class.call(differences: differences) }
  let(:differences) do
    [
      {
        ext_ref: 1,
        internal: campaign_1,
        external: {
          ad_description: "this is a test",
          external_reference: 1,
          status: Campaign::ACTIVE
        },
        differences: [],
      }.with_indifferent_access,
      {
        ext_ref: 2,
        internal: campaign_2,
        external: {
          ad_description: "this is a test too",
          external_reference: 2,
          status: Campaign::ACTIVE
        },
        differences: ['status'],
      }.with_indifferent_access
    ]
  end

  let!(:campaign_1) { create(:campaign, campaign_1_data) }
  let!(:campaign_2) { create(:campaign, campaign_2_data) }
  let(:campaign_1_data) { { external_reference: 1, status: Campaign::ACTIVE, ad_description: "this is a test" } }
  let(:campaign_2_data) { { external_reference: 2, status: Campaign::PAUSED, ad_description: "this is a test too" } }
  let(:expected_result) do
    [
      {
        remote_reference: 2,
        discrepanices: [{ status: { internal: Campaign::PAUSED, remote: Campaign::ACTIVE }}],
      }
    ]
  end

  it 'returns the expected result' do
    expect(display_differences).to match(expected_result)
  end
end
